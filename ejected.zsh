# Ejeceted theme for Zsh
#
# Author: Max Audron <audron@ejected.space>
# Repository: https://github.com/ejected/zsh-theme
# License: MIT

export VIRTUAL_ENV_DISABLE_PROMPT=1
function virtualenv_info {
    [ "$VIRTUAL_ENV" ] && echo '%F{8} '"%\uf00c$(basename "$VIRTUAL_ENV")"%f' '
}

setopt prompt_subst

autoload -Uz vcs_info

zstyle -e ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:*' get-revision true
zstyle ':vcs_info:git:*' formats '%F{8} %b $(git_remote_check)%m%c%u'
zstyle ':vcs_info:git*+set-message:*' hooks git-status

+vi-git-untracked(){
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        git status --porcelain | grep '??' &> /dev/null ; then
		hook_com[staged]+='T'
	fi
}


+vi-git-status() {
  git update-index -q --ignore-submodules --refresh >/dev/null
  if ! git diff-index --cached --quiet HEAD --ignore-submodules -- >/dev/null; then
	hook_com[staged]='%F{2}  '
	if git status -s | grep "??" >/dev/null; then
      hook_com[unstaged]='%F{2} '
    else
      hook_com[unstaged]=''
    fi
  else
	hook_com[staged]=''
    if git status -s | grep "??" >/dev/null; then
      hook_com[unstaged]='%F{2} '
    else
	  hook_com[unstaged]=''
    fi
  fi
}

git_remote_check() {
  local_commit=$(git rev-parse "@" 2>/dev/null)
  remote_commit=$(git rev-parse "@{u}" 2>/dev/null)

  if [[ $local_commit == "@" || $local_commit == $remote_commit ]]; then
    echo ""
  else
    common_base=$(git merge-base "@" "@{u}" 2>/dev/null) # last common commit
    if [[ $common_base == $remote_commit ]]; then
      echo "%F{green}⇡ "
    elif [[ $common_base == $local_commit ]]; then
      echo "%F{red}⇣ "
    else
      echo "%F{green}⇡ %F{red}⇣ "
    fi
  fi
}

precmd(){
	vcs_info
	local preprompt_left="%F{2}%3~ ${vcs_info_msg_0_} $(virtualenv_info)"
    local preprompt_right="%F{8}%n@%M"
    local preprompt_left_length=${#${(S%%)preprompt_left//(\%([KF1]|)\{*\}|\%[Bbkf])}}
    local preprompt_right_length=${#${(S%%)preprompt_right//(\%([KF1]|)\{*\}|\%[Bbkf])}}
    local num_filler_spaces=$((COLUMNS - preprompt_left_length - preprompt_right_length))
    print -Pr $'\n'"$preprompt_left${(l:$num_filler_spaces:)}$preprompt_right"
}
PS1="%(?.%F{8}.%F{red})> %f "
RPS1=""
