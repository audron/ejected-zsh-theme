# Ejected ZSH Prompt

![alt text](https://ejected.space/assets/ejected-zsh-theme.png "ejected-zsh-theme")

### A simple and fast zsh theme
Features:
* 3 Level Path
* Git branch and push/pull
* Git file status
* python virtualenv
* User@hostname on right side

The git file status is split into 2 circles:
*   Shows up Red if there are uncommited but added files
*   Shows up Red if there are unstaged files

